var webpack = require('webpack');
var path = require('path');

const config = {
    debug: true,
    devtool: 'source-map',
    noInfo: false,
    target: 'web',

    entry: [
		'webpack-hot-middleware/client?reload=true',
		path.resolve(__dirname, 'src/index.tsx')
	],


    output: {
		path: __dirname + '/dist',
		publicPath: '/',
        filename: 'bundle.js'
    },

	devServer: {
		contentBase: path.resolve(__dirname, 'src')
	},

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	],

    module: {
        loaders: [
            { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
            { test: /\.js$/, loader: 'source-map-loader' },
			// { test: /\.ts$/, loader: 'tslint-loader' },
			// { test: /\.tsx$/, loader: 'tslint-loader' },
			{ test: /(\.css)$/, loaders: ['style', 'css']},
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
			{ test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000'},
			{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
			{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
        ]
    },

	tslint: {
		failOnHint: true,
	   	configuration: require('./tslint.json')
	}

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    // externals: {
    //     'react': 'React',
    //     'react-dom': 'ReactDOM'
    // },
};

export default config;
