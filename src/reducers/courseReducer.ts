import { ActionType, DispatchAction } from './../actions/ActionTypes';
import { ICourse } from '../api/mockCourseApi';
import initialState from './state';

export default function coursesReducer(state: ICourse[] = initialState.courses, dispatch: DispatchAction): ICourse[]
{
    switch(dispatch.type)
    {
        case ActionType.LOAD_COURSES_SUCCESS:
            return dispatch.payload;

        case ActionType.CREATE_COURSE_SUCCESS:
            return [
                ...state,
                Object.assign({}, dispatch.payload)
            ];

        case ActionType.UPDATE_COURSE_SUCCESS:
            return [
                ...state.filter(c => c.id !== dispatch.payload.id),
                Object.assign({}, dispatch.payload)
            ];

        default:
            return state;
    }
}
