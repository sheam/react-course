import { ICourse } from '../api/mockCourseApi';
import { IAuthor } from '../api/mockAuthorApi';

export interface ICoursesState { courses?: ICourse[]; }
export interface IAuthorsState { authors?: IAuthor[]; }
export interface IAjaxState { ajaxCallsInProgress?: number; }
export interface IManageCourseManagementState { course?: ICourse; errors?: any; saving?: boolean; }

export interface IState extends ICoursesState, IAuthorsState, IAjaxState, IManageCourseManagementState {}

const initialState: IState =
{
    courses: [],
    authors: [],
    ajaxCallsInProgress: 0
};

export default initialState;
