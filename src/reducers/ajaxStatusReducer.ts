import { ActionType, DispatchAction } from './../actions/ActionTypes';
import initialState from './state';

function isSuccess(t: ActionType): boolean
{
    return t.toString().endsWith('_SUCCESS');
}

export default function ajaxStatusReducer(state: number = initialState.ajaxCallsInProgress, dispatch: DispatchAction): number
{
    if(ActionType.BEGIN_AJAX_CALL == dispatch.type)
    {
        return state + 1;
    }
    else if(ActionType.AJAX_CALL_ERROR == dispatch.type || isSuccess(dispatch.type))
    {
        return state - 1;
    }

    return state;
}

