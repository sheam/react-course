import { ActionType, DispatchAction } from './../actions/ActionTypes';
import { IAuthor } from '../api/mockAuthorApi';
import initialState from './state';

export default function authorReducer(state: IAuthor[] = initialState.authors, dispatch: DispatchAction): IAuthor[]
{
    switch(dispatch.type)
    {
        case ActionType.LOAD_AUTHORS_SUCCESS:
            return dispatch.payload;

        default:
            // console.log(`returning initial state, because action is [${dispatch.type}].`);
            return state;
    }
}
