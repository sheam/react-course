import * as React from 'react';
import {Link} from 'react-router';

export class HelloPage extends React.Component<undefined, undefined>
{
    public render()
    {
        return (
          <div className='jumbotron'>
            <h1>Hello There!</h1>
            <p>
                Go here to learn more
                { ' ' }
                <Link to='about'>about</Link> the application.
            </p>
          </div>
        );
    }
}

