import * as React from 'react';
import { ICourse } from '../../api/mockCourseApi';
import { Link } from 'react-router';

export interface ICourseListRowProps { course: ICourse }

export class CourseListRow extends React.Component<ICourseListRowProps, undefined>
{
    public render(): JSX.Element
    {
        const c: ICourse = this.props.course;
        return (
            <tr>
                <th><a href={c.watchHref} target='_blank'>Watch</a></th>
                <th><Link to={'/course/' + c.id}>{c.title}</Link></th>
                <th>{c.authorId}</th>
                <th>{c.category}</th>
                <th>{c.length}</th>
            </tr>
        );
    }
}
