import * as React from 'react';
import { ICourse } from '../../api/mockCourseApi';
import { IAuthor } from '../../api/mockAuthorApi';
import { TextInput } from '../common/TextInput';
import { SelectInput } from '../common/SelectInput';
import { IOption } from '../common/SelectInput';

export interface ICourseFormProps
{
    course: ICourse;
    allAuthors?: IOption[];
    onSave: (e: React.FormEvent<any>) => void;
    onChange?: (e: React.FormEvent<any>) => void;
    saving?: boolean;
    errors?: any;
}

export class CourseForm extends React.Component<ICourseFormProps, undefined>
{
    public static get propTypes(): any
    {
        return {
            course: React.PropTypes.object.isRequired,
            allAuthors: React.PropTypes.array,
            onSave: React.PropTypes.func.isRequired,
            onChange: React.PropTypes.func,
            saving: React.PropTypes.bool,
            errors: React.PropTypes.object
        };
    }

    public render(): JSX.Element
    {
        const course = this.props.course;
        const allAuthors = this.props.allAuthors;
        const onSave = this.props.onSave;
        const onChange = this.props.onChange;
        const saving = this.props.saving;
        const errors = this.props.errors;

        return (
            <form>
              <h1>Manage Course</h1>
              <TextInput
                name='title'
                label='Title'
                value={course.title}
                onChange={onChange}
                error={errors.title}/>

              <SelectInput
                name='authorId'
                label='Author'
                value={course.authorId}
                defaultOption='Select Author'
                options={allAuthors}
                onChange={onChange} error={errors.authorId}/>

              <TextInput
                name='category'
                label='Category'
                value={course.category}
                onChange={onChange}
                error={errors.category}/>

              <TextInput
                name='length'
                label='Length'
                value={course.length}
                onChange={onChange}
                error={errors.length}/>

              <input
                type='submit'
                disabled={saving}
                value={saving ? 'Saving...' : 'Save'}
                className='btn btn-primary'
                onClick={onSave}/>
            </form>
        );
    }
}
