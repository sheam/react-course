import * as React from 'react';
import { ICourse } from '../../api/mockCourseApi';
import { CourseListRow } from './CourseListRow';

export interface ICourseListProps { courses: ICourse[] }

export class CourseList extends React.Component<ICourseListProps, undefined>
{
    public render(): JSX.Element
    {
        return (
            <table className='table'>
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Category</th>
                    <th>Length</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.props.courses.map(c =>
                        <CourseListRow key={c.id} course={c} />)
                }
                </tbody>
            </table>
        );
    }
}
