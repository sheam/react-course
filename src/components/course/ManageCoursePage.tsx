import * as React from 'react';
import { connect } from 'react-redux';
import { DispatchAction } from '../../actions/ActionTypes';
import * as ca from '../../actions/courseActions';
import { ICourse } from '../../api/mockCourseApi';
import { IAuthor } from '../../api/mockAuthorApi';
import { CourseForm } from './CourseForm';
import { IOption } from '../common/SelectInput';
import * as toastr from 'toastr';
import { ICoursesState, IAuthorsState, IManageCourseManagementState } from '../../reducers/state';

export interface IManageCoursePageProps
{
    course: ICourse;
    authors: IOption[];
}

export interface IOwnProps
{
    params: { id: string }
}

export interface IManageCoursePageState extends IManageCourseManagementState, IAuthorsState, ICoursesState {}

class ManageCoursePage extends React.Component<IManageCoursePageProps & ca.ICourseActions, IManageCoursePageState>
{
    public constructor(props: IManageCoursePageProps, context: any)
    {
        super(props, context);
        this.state =
        {
            course: Object.assign({}, this.props.course) as ICourse,
            errors: {},
            saving: false,
        };
    }

    public componentWillReceiveProps(nextProps: IManageCoursePageProps): void
    {
        if(this.props.course.id != nextProps.course.id)
        {
            this.setState(
            {
                course: Object.assign({}, nextProps.course)
            });
        }
        //TODO: unimplemented componentDidMount
    }

    private updateCourseState(event: React.FormEvent<any>): void
    {
        const field = (event.target as any).name;
        const course = this.state.course as any;
        course[field] = (event.target as any).value;
        return this.setState({course: course});
    }

    private saveCourse(event: React.FormEvent<any>): void
    {
        event.preventDefault();
        this.setState({ saving: true });
        this.props.saveCourse(this.state.course)
            .then(() => this.redirect())
            .catch((e: any) =>
            {
                this.setState({ saving: false });
                toastr.error(e);
            });
    }

    private redirect(): void
    {
        this.setState({ saving: false });
        toastr.success('Course saved');
        this.context.router.push('/courses');
    }

    public static get contextTypes(): any
    {
        return {
            router: React.PropTypes.object
        };
    }

    public render(): JSX.Element
    {
        return (
            <CourseForm
                allAuthors={this.props.authors}
                course={this.state.course}
                errors={this.state.errors}
                onSave={(e: React.FormEvent<any>) => this.saveCourse(e)}
                onChange={(e: React.FormEvent<any>) => this.updateCourseState(e)}
                saving={this.state.saving}
            />
        );
    }
}

const getCourseById = (courses: ICourse[], id: string): ICourse =>
{
    const course = courses.filter((c: ICourse) => c.id === id);
    return course ? course[0] : null;
};

const mapStateToProps = (state: IManageCoursePageState, ownProps: IOwnProps): IManageCoursePageProps =>
{
    const courseId = ownProps.params.id; //because routes.tsx has '/course/:id'
    let course: ICourse = { id: '', watchHref: '', title: '', authorId: '', category: '', length: '0:00' };
    if(courseId && state.courses.length > 0)
    {
        course = getCourseById(state.courses, courseId);
    }

    const authorOptions: IOption[] = state.authors.map((a: IAuthor) =>
    {
        return {
            value: a.id,
            text: `${a.firstName} ${a.lastName}`
        };
    });

    return {
        course: course,
        authors: authorOptions
    };
};

const mapDispatchToProps = (dispatch: (a: any) => DispatchAction): ca.ICourseActions =>
{
    return {
        saveCourse: (c: ICourse) => dispatch(ca.saveCourse(c))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageCoursePage as any);
