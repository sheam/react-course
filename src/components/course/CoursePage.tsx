import * as React from 'react';
import { connect } from 'react-redux';
import { DispatchAction } from '../../actions/ActionTypes';
import * as ca from '../../actions/courseActions';
import { ICourse } from '../../api/mockCourseApi';
import { CourseList } from './CourseList';
import { browserHistory } from 'react-router';
import { ICoursesState } from '../../reducers/state';

export interface ICoursePageProps extends ICoursesState {}
export interface ICoursePageState extends ICoursesState {}

class CoursePage extends React.Component<ICoursePageProps & ca.ICourseActions, undefined>
{
    private redirectToAddCoursePage(event: React.FormEvent<any>): void
    {
        browserHistory.push('/course');
    }

    public render(): JSX.Element
    {
        return (
            <div>
                <h1>Courses</h1>
                <input
                    type='submit'
                    value='Add Course'
                    className='btn btn-primary'
                    onClick={(e: React.FormEvent<any>) => this.redirectToAddCoursePage(e)}
                />
                <CourseList courses={this.props.courses} />
            </div>
        );
    }
}

const mapStateToProps = (state: ICoursePageState): ICoursePageProps =>
{
    return {
        courses: state.courses
    };
};

const mapDispatchToProps = (dispatch: (a: any) => DispatchAction): ca.ICourseActions =>
{
    return {
        loadCoursesSuccess: (c: ICourse[]) => dispatch(ca.loadCoursesSuccess(c))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CoursePage as any);
