import * as React from 'react';

export interface ISelectInputProps
{
    name: string;
    label: string;
    onChange: (e: React.FormEvent<any>) => void;
    defaultOption?: string;
    value?: string;
    error?: string;
    options?: IOption[];
}

export interface IOption
{
    text: string;
    value: string;
}

export class SelectInput extends React.Component<ISelectInputProps, undefined>
{
    public static get propTypes(): any
    {
        return {
            name: React.PropTypes.string.isRequired,
            label: React.PropTypes.string.isRequired,
            onChange: React.PropTypes.func.isRequired,
            defaultOption: React.PropTypes.string,
            value: React.PropTypes.string,
            error: React.PropTypes.string,
            options: React.PropTypes.arrayOf(React.PropTypes.object)
        };
    }

    public render(): JSX.Element
    {
        const name = this.props.name;
        const label = this.props.label;
        const onChange = this.props.onChange;
        const defaultOption = this.props.defaultOption;
        const value = this.props.value;
        const error = this.props.error;
        const options = this.props.options;

        return (
            <div className='form-group'>
              <label htmlFor={name}>{label}</label>
              <div className='field'>
                {/* Note, value is set here rather than on the option - docs: https://facebook.github.io/react/docs/forms.html */}
                <select
                  name={name}
                  value={value}
                  onChange={onChange}
                  className='form-control'>
                  <option value=''>{defaultOption}</option>
                  {options.map((option) => {
                    return <option key={option.value} value={option.value}>{option.text}</option>;
                  })
                  }
                </select>
                {error && <div className='alert alert-danger'>{error}</div>}
              </div>
            </div>
        );
    }
}
