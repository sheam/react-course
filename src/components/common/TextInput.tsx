import * as React from 'react';

export interface ITextInputProps
{
    name: string;
    label: string;
    onChange: (e: React.FormEvent<any>) => void;
    placeholder?: string;
    value?: string;
    error?: string;
}

export class TextInput extends React.Component<ITextInputProps, undefined>
{
    public constructor(props: ITextInputProps, context: any)
    {
        super(props);
    }

    public static get propTypes(): any
    {
        return {
            name: React.PropTypes.string.isRequired,
            label: React.PropTypes.string.isRequired,
            onChange: React.PropTypes.func.isRequired,
            placeholder: React.PropTypes.string,
            value: React.PropTypes.string,
            error: React.PropTypes.string
        };
    }

    public render(): JSX.Element
    {
        const error = this.props.error;
        const label = this.props.label;
        const name = this.props.name;
        const placeholder = this.props.placeholder;
        const value = this.props.value;
        const onChange = this.props.onChange;

        let wrapperClass = 'form-group';
        if (error && error.length > 0)
        {
            wrapperClass += ' ' + 'has-error';
        }

        return (
          <div className={wrapperClass}>
            <label htmlFor={name}>{label}</label>
            <div className='field'>
              <input
                type='text'
                name={name}
                className='form-control'
                placeholder={placeholder}
                value={value}
                onChange={onChange}/>
              {error && <div className='alert alert-danger'>{error}</div>}
            </div>
          </div>
        );

    }
}
