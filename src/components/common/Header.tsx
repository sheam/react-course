import * as React from 'react';
import {Link, IndexLink} from 'react-router';
import { LoadingDots } from './LoadingDots';

export interface IHeaderProps { loading: boolean }

export class Header extends React.Component<IHeaderProps, undefined>
{
    public render()
    {
        return (
        <nav className='navbar navbar-default'>
          <div className='container-fluid'>
              <a href='/' className='navbar-brand'>
                <img src='images/pluralsight-logo.png' />
              </a>
              <ul className='nav navbar-nav'>
                <li><IndexLink activeClassName='active' to='/'>Home</IndexLink></li>
                <li><Link activeClassName='active' to='/courses'>Courses</Link></li>
                <li><Link activeClassName='active' to='/oldauthors'>Old Authors</Link></li>
                <li><Link activeClassName='active' to='/about'>About</Link></li>
              </ul>
          </div>
          { this.props.loading && <LoadingDots interval={100} dots={80} /> }
        </nav>
        );
    }
}


