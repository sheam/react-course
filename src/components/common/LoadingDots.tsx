import * as React from 'react';

export interface ILoadingDotsProps { interval?: number, dots?: number }
export interface ILoadingDotsState { frame: number }

export class LoadingDots extends React.Component<ILoadingDotsProps, ILoadingDotsState>
{
    private interval: number;

    public constructor(props: ILoadingDotsProps, context: any)
    {
        super(props, context);
        this.state = { frame: 1 };
    }

    public componentDidMount(): void
    {
        this.interval = window.setInterval(() =>
        {
            this.setState({ frame: this.state.frame + 1 });
        }, this.props.interval);
    }

    public componentWillUnmount(): void
    {
        if(this.interval)
        {
            window.clearInterval(this.interval);
        }
    }

    public static get defaultProps(): ILoadingDotsProps
    {
        return {
            interval: 300,
            dots: 3
        };
    }

    public render(): JSX.Element
    {
        let dots = this.state.frame % (this.props.dots + 1);
        let text = '';
        while (dots > 0)
        {
          text += '.';
          dots--;
        }
        return <span>{text}&nbsp;</span>;
    }
}
