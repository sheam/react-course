import * as React from 'react';
import * as _ from 'lodash';
import OldAuthorApi = require('../../api/authorApi.js');

export interface IOldAuthor { id: string, firstName: string, lastName: string };
export interface IOldAuthorProps { authors: IOldAuthor[] };

export class OldAuthorList extends React.Component<IOldAuthorProps, undefined>
{
    private createAuthorRow(author: IOldAuthor, i: number)
    {
        return (
            <tr key={author.id}>
                <td>
                    {i+1}
                </td>
                <td>
                    <a href={'/#authors/' + author.id}>{author.id}</a>
                </td>
                <td>
                    {author.firstName} {author.lastName}
                </td>
            </tr>
        );
    }

    public static get propTypes(): any
    {
        return {
            authors: React.PropTypes.array.isRequired
        };
    }

    public static get defaultProps(): IOldAuthorProps
    {
        return {
            authors: []
        };
    }

    public render(): JSX.Element
    {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.authors.map(this.createAuthorRow, this )}
                    </tbody>
                </table>
            </div>
        );
    }
}
