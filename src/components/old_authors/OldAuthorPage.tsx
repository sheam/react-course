import * as React from 'react';
import * as _ from 'lodash';
import OldAuthorApi = require('../../api/authorApi.js');

import { OldAuthorList } from './OldAuthorList';
import { IOldAuthorProps } from './OldAuthorList';
import { IOldAuthor } from './OldAuthorList';

interface IOldAuthorPageState { authors: IOldAuthor[] };

export class OldAuthorPage extends React.Component<undefined, IOldAuthorPageState>
{
    constructor(props: any)
    {
        super(props);
        this.state = { authors: [] };
    }

    public componentDidMount(): void
    {
        const data = OldAuthorApi.getAllAuthors() as IOldAuthor[];
        this.setState({ authors: data });
    }

    public render(): JSX.Element
    {
        return (
            <div>
                <h1>Old Authors</h1>
                <OldAuthorList authors={this.state.authors} />
            </div>
        );
    }
}
