import * as React from 'react';
import {connect} from 'react-redux';

import { Header } from './common/Header';
import { IState } from '../reducers/state';

export interface IApplicationProps
{
    children?: React.Component<any, any>,
    loading?: boolean
}

export class Application extends React.Component<IApplicationProps, undefined>
{
    public static get propTypes(): any
    {
        return {
            children: React.PropTypes.object.isRequired
        };
    }

    public render(): JSX.Element
    {
        return (
            <div className='fluid-container'>
                <Header loading={this.props.loading} />
                {this.props.children}
            </div>
        );
    }
}

const mapStateToProps = (state: IState, ownProps: any): IApplicationProps =>
{
    return {
        loading: state.ajaxCallsInProgress > 0
    };
};

export default connect(mapStateToProps)(Application as any);
