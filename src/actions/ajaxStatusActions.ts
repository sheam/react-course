import { ActionType, DispatchAction } from './ActionTypes';

export function beginAjaxCall(): DispatchAction
{
    return { type: ActionType.BEGIN_AJAX_CALL };
}

export function ajaxCallError(error: string): DispatchAction
{
    return { type: ActionType.AJAX_CALL_ERROR, error: true, payload: error };
}
