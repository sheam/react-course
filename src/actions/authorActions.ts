import { AuthorApi, IAuthor } from '../api/mockAuthorApi';
import { ActionType, DispatchAction } from './ActionTypes';
import {beginAjaxCall} from './ajaxStatusActions';

export interface IAuthorActions
{
    loadAuthorsSuccess?: (c: IAuthor[]) => DispatchAction
}

export function loadAuthorsSuccess(authors: IAuthor[]): DispatchAction
{
    return { type: ActionType.LOAD_AUTHORS_SUCCESS, payload: authors };
}

export function loadAuthors()
{
    return function(dispatch: (a: DispatchAction) => void)
    {
        dispatch(beginAjaxCall());
        return AuthorApi
            .getAllAuthors()
            .then((authors: IAuthor[]) =>
            {
                dispatch(loadAuthorsSuccess(authors));
            })
            .catch((error: any) =>
            {
                throw(error);
            });
    };
}

