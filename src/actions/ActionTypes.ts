import { ICourse } from '../api/mockCourseApi';
import { IAuthor } from '../api/mockAuthorApi';

export enum ActionType
{
    BEGIN_AJAX_CALL = <any>'BEGIN_AJAX_CALL',
    AJAX_CALL_ERROR = <any>'AJAX_CALL_ERROR',

    LOAD_COURSES_SUCCESS = <any>'LOAD_COURSES_SUCCESS',
    LOAD_AUTHORS_SUCCESS = <any>'LOAD_AUTHORS_SUCCESS',
    CREATE_COURSE_SUCCESS = <any>'CREATE_COURSE_SUCCESS',
    UPDATE_COURSE_SUCCESS = <any>'UPDATE_COURSE_SUCCESS',
}

interface IDA<T> { type: ActionType, error?: boolean, payload?: T }
interface IAjaxEventAction extends IDA<any> {}
interface ICoursesAction extends IDA<ICourse[]> {}
interface ICourseAction extends IDA<ICourse> {}
interface IAuthorsAction extends IDA<IAuthor[]> {}
interface IAuthorAction extends IDA<IAuthor> {}
export type DispatchAction = IAjaxEventAction|ICourseAction|ICoursesAction|IAuthorAction|IAuthorsAction;
