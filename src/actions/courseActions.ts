import { CourseApi, ICourse } from '../api/mockCourseApi';
import { ActionType, DispatchAction } from './ActionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export interface ICourseActions
{
    loadCoursesSuccess?: (c: ICourse[]) => DispatchAction
    saveCourse?: (c: ICourse) => any;
}

export function loadCoursesSuccess(courses: ICourse[]): DispatchAction
{
    return { type: ActionType.LOAD_COURSES_SUCCESS, payload: courses };
}

export function updateCourseSuccess(course: ICourse): DispatchAction
{
    return { type: ActionType.UPDATE_COURSE_SUCCESS, payload: course };
}

export function createCourseSuccess(course: ICourse): DispatchAction
{
    return { type: ActionType.CREATE_COURSE_SUCCESS, payload: course };
}

export function loadCourses(): (dispatch: (a: DispatchAction) => void) => Promise<void>
{
    return function(dispatch: (a: DispatchAction) => void)
    {
        dispatch(beginAjaxCall());
        return CourseApi
            .getAllCourses()
            .then((courses: ICourse[]) =>
            {
                return dispatch(loadCoursesSuccess(courses));
            })
            .catch((error: any) => { throw(error); });
    };
}

export function saveCourse(course: ICourse): (dispatch: (a: DispatchAction) => void) => Promise<void>
{
    return function(dispatch: (a: DispatchAction) => void)
    {
        dispatch(beginAjaxCall());
        return CourseApi
            .saveCourse(course)
            .then((savedCourse) =>
            {
                course.id ?
                    dispatch(updateCourseSuccess(savedCourse)) :
                    dispatch(createCourseSuccess(savedCourse));
            })
            .catch((error: any) =>
            {
                dispatch(ajaxCallError(error));
                throw(error);
            });
    };
}
