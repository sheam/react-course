import * as React from 'react';
import { createStore, applyMiddleware, Store, GenericStoreEnhancer } from 'redux';
import rootReducer from '../reducers';
import * as reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';

export default function configureStore(initialState?: any): Store<GenericStoreEnhancer>
{
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunk, reduxImmutableStateInvariant())
    );
}
