import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Router from 'react-router';
import { Route, IndexRoute } from 'react-router';

import  Application  from './components/Application';
import { AboutPage } from './components/about/AboutPage';
import { HelloPage } from './components/hello/HelloPage';
import { OldAuthorPage } from './components/old_authors/OldAuthorPage';
import  CoursePage  from './components/course/CoursePage';
import  ManageCoursePage  from './components/course/ManageCoursePage';

const routes = (
    <Route path='/' component={Application}>
        <IndexRoute component={HelloPage} />
        <Route path='about' component={AboutPage} />
        <Route path='oldauthors' component={OldAuthorPage} />
        <Route path='courses' component={CoursePage} />
        <Route path='course/:id' component={ManageCoursePage} />
        <Route path='course' component={ManageCoursePage} />
    </Route>
);

export default routes;
