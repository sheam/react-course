'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect'); //runs local dev server
var open = require('gulp-open'); //opens url in browser
var webpack = require('gulp-webpack');
var concat = require('gulp-concat');

var config = {
	port: 9005,
	devBaseUrl: 'http://localhost',
	paths: {
		ts: ['./src/**/*.ts', './src/**/*.tsx'],
		html: './src/*.html',
		dist: './dist',
        images: './src/images/*/',
		css: [
			'node_modules/bootstrap/dist/css/bootstrap.min.css',
			'node_modules/bootstrap/dist/css/bootstrap-theme.min.css'
		]
	}
};

//start server
gulp.task('connect', () =>
{
	connect.server({
		root: [config.paths.dist],
		port: config.port,
		base: config.devBaseUrl,
		livereload: true
	});
});

gulp.task('open', ['connect'], () =>
{
	gulp.src('dist/index.html')
		.pipe(open({uri: config.devBaseUrl + ':' + config.port + '/'}));
});


gulp.task('html', () =>
{
	gulp.src(config.paths.html)
		.pipe(gulp.dest(config.paths.dist))
		.pipe(connect.reload());
});

gulp.task('watch', () =>
{
	gulp.watch(config.paths.html, ['html']);
    gulp.watch(config.paths.ts, ['js']);
});

gulp.task('css', () =>
{
	gulp.src(config.paths.css)
	    .pipe(concat('bundle.css'))
		.pipe(gulp.dest(config.paths.dist + '/css'));
});

// Migrates images to dist folder
// Note that I could even optimize my images here
gulp.task('images', function () {
    gulp.src(config.paths.images)
        .pipe(gulp.dest(config.paths.dist + '/images'))
        .pipe(connect.reload());

    //publish favicon
    gulp.src('./src/favicon.ico')
        .pipe(gulp.dest(config.paths.dist));
});

gulp.task('js', () =>
{
	return gulp.src(config.paths.ts)
		.pipe(webpack( require('./webpack.config.js') ))
		.pipe(gulp.dest(config.paths.dist + '/scripts'))
		.pipe(connect.reload());
});

gulp.task('build', ['html', 'js', 'css', 'images']);

gulp.task('run', ['build', 'open', 'watch']);
